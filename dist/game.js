"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// copied from: https://github.com/digitsensitive/phaser3-typescript/tree/master/src/boilerplate
require("phaser");
const main_scene_1 = require("./scenes/main.scene");
// main game configuration
const config = {
    width: 800,
    height: 600,
    type: Phaser.AUTO,
    parent: 'game',
    scene: main_scene_1.MainScene,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 200 }
        }
    }
};
// game class
class Game extends Phaser.Game {
    constructor(conf) {
        super(conf);
    }
}
exports.Game = Game;
// when the page is loaded, create our game instance
window.onload = () => {
    const game = new Game(config);
};
//# sourceMappingURL=game.js.map