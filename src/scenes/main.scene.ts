import { Pepino } from '../elements/player/pepino';

/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @license      Digitsensitive
 */

export class MainScene extends Phaser.Scene {
  private pepino: Pepino;
  constructor() {
    super({
      key: 'MainScene'
    });
  }

  preload(): void {
    this.load.image('imagenTiles', '/assets/maps/mapaZaida/tilesetPepino.png');
    this.load.image('pepino', '/assets/pepinoplayer.png');
    this.load.tilemapTiledJSON(
      'map',
      '/assets/maps/mapaZaida/mapaDos.json'
    );
  }

  create(): void {
    const map = this.make.tilemap({ key: 'map' });
    const tiles = map.addTilesetImage('platformTiles', 'imagenTiles');
    const platformLayer = map.createDynamicLayer(
      'capa',
      tiles,
      null,
      null
    );
    platformLayer.setCollisionByProperty({ collides: true });
    // platformLayer.setCollisionByExclusion([-1]);
    this.pepino = new Pepino(this, 64, 64);
    this.physics.world.collideSpriteVsTilemapLayer(
      this.pepino.sprite,
      platformLayer,
      null,
      null,
      null,
      false
    );

    this.physics.add.collider(platformLayer, this.pepino.sprite);

    // this.physics.world.addCollider(this.pepino.sprite, platformLayer);

    // const layer = map.createStaticLayer(0, tiles, 0, 0);

    this.cameras.main.startFollow(
      this.pepino.sprite,
      false,
      1,
      1,
      -window.innerWidth / 8,
      -window.innerHeight / 4
    );
  }

  update(time: number, delta: number) {
    this.pepino.update();
  }
}
