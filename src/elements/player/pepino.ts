export class Pepino {
  public scene: Phaser.Scene;
  public sprite: Phaser.GameObjects.Sprite;
  public keys;
  constructor(scene, x, y) {
    this.scene = scene;

    // Create the animations we need from the player spritesheet
    const anims = this.scene.anims;
    /*anims.create({
            key: 'player-idle',
            frames: anims.generateFrameNumbers('player', { start: 0, end: 3 }),
            frameRate: 3,
            repeat: -1
        });
        anims.create({
            key: 'player-run',
            frames: anims.generateFrameNumbers('player', { start: 8, end: 15 }),
            frameRate: 12,
            repeat: -1
        });*/

    // Create the physics-based sprite that we will move around and animate
    this.sprite = this.scene.physics.add
      .sprite(x, y, 'pepino', 0)
      .setScale(0.05, 0.051)
      .setDrag(1000, 0)
      .setMaxVelocity(300, 400)
      .setSize(20, 20)
      .setOffset(4, 400)
      .setCollideWorldBounds(true);

    // Track the arrow keys & WASD
    const { LEFT, RIGHT, UP, W, A, D } = Phaser.Input.Keyboard.KeyCodes;
    this.keys = this.scene.input.keyboard.addKeys({
      left: LEFT,
      right: RIGHT,
      up: UP,
      w: W,
      a: A,
      d: D
    });
    (this.sprite.body as Phaser.Physics.Arcade.Body).setCollideWorldBounds(
      true
    );
  }

  freeze() {
    (this.sprite.body as Phaser.Physics.Arcade.Body).moves = false;
  }

  update() {
    // const { keys, sprite } = this;
    const onGround = (this.sprite.body as Phaser.Physics.Arcade.Body).blocked
      .down;
    const acceleration = onGround ? 600 : 200;

    // Apply horizontal acceleration when left/a or right/d are applied
    if (this.keys.left.isDown || this.keys.a.isDown) {
      (this.sprite.body as Phaser.Physics.Arcade.Body).setAccelerationX(
        -acceleration
      );
      // No need to have a separate set of graphics for running to the left & to the right. Instead
      // we can just mirror the sprite.
      this.sprite.setFlipX(true);
    } else if (this.keys.right.isDown || this.keys.d.isDown) {
      (this.sprite.body as Phaser.Physics.Arcade.Body).setAccelerationX(
        acceleration
      );
      this.sprite.setFlipX(false);
    } else {
      (this.sprite.body as Phaser.Physics.Arcade.Body).setAccelerationX(0);
    }

    // Only allow the player to jump if they are on the ground
    if (onGround && (this.keys.up.isDown || this.keys.w.isDown)) {
      (this.sprite.body as Phaser.Physics.Arcade.Body).setVelocityY(-300);
    }

    // Update the animation/texture based on the state of the player
    /*if (onGround) {
            if ((this.sprite.body as Phaser.Physics.Arcade.Body).velocity.x !== 0) {
                this.sprite.anims.play('player-run', true);
            } else {
                this.sprite.anims.play('player-idle', true);
            }
        } else {
            this.sprite.anims.stop();
            this.sprite.setTexture('player', 10);
        }*/
  }

  destroy() {
    this.sprite.destroy();
  }
}
