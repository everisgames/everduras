// copied from: https://github.com/digitsensitive/phaser3-typescript/tree/master/src/boilerplate
import 'phaser';
import { MainScene } from './scenes/main.scene';

// main game configuration
const config: GameConfig = {
  width: 1920,
  height: 1080,
  type: Phaser.AUTO,
  parent: 'game',
  scene: MainScene,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 400 }
    }
  }
};

// game class
export class Game extends Phaser.Game {
  constructor(conf: GameConfig) {
    super(conf);
  }
}

// when the page is loaded, create our game instance
window.onload = () => {
  const game = new Game(config);
};
